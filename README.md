# E-commerce Web App (Code: SDN)

## Summary
This is a full-stack e-commerce web application for selling electronic items, built using the MERN stack (MongoDB, ExpressJS, ReactJS, NodeJS) over 3 months. The backend follows a Layered Architecture with the following structure:

- **Router Layer**: Handles incoming HTTP requests and routes them to the appropriate controller.
- **Controller Layer**: Processes requests, interacts with services, and sends responses.
- **Service Layer**: Contains business logic, processes data, and manages the e-commerce workflows.
- **Model Layer**: Interacts with MongoDB for data storage and retrieval.

![App Screenshot](./image_capture/1c1e09ae-cc63-4480-8655-749d87875f37.png)

## Technologies
### Frontend
- **Frameworks/Libraries**: ReactJS, Redux, Material-UI, TypeScript

### Backend
- **Frameworks/Libraries**: NodeJS, ExpressJS, Mongoose, JWT, Socket.IO

### Database
- MongoDB

### Tools
- MongoDB Compass
- Postman
- Jira

## Features
- **General**: User authentication (Login, Register, Confirm Email, Forgot Password), Profile Editing
- **Guest Users**: Browse products, Search with filters and pagination
- **Customers**: Add products to cart, Checkout with QR code
- **Sales Manager**: Payment management, Export monthly sales reports in XML format
- **Admin**: Product management

## Team Members
- **TienPV**: Project Manager
- **NgocNB**: Developer
- **HiepTH**: Developer
- **LongNH**: Business Analyst

## Notes
- The project uses a trial version of the MongoDB cluster, which may lead to potential database accessibility issues in the future.
- Configuration for the `.env` files can be found in the constants files in both the backend and frontend directories.
- There are unresolved bugs with Redux on the frontend that have been identified but may not be committed yet. Apologies for any inconvenience this may cause.
- The frontend runs on port 3000, the backend on port 3333, and the MongoDB database uses the default port 27017.
- You can view the completed product in the `Presentation.pdf` file.

## Installation
### Frontend
To install and run the frontend in production mode:
```sh
cd frontend
npm install
npm start
```

### Backend
To install and run the backend in production mode:
```sh
cd backend
npm install
npm start
```